<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: text/event-stream\n");
date_default_timezone_set("UTC"); 

while (1) {
    $rcvtime = "";
    $delay = 1;

    if (!empty($_GET)) {
        $rcvtime = $_GET['time'];
        $delay = $_GET['delay'];
    }

    $curtime = time();
    $data = array(
        "curtime" => $curtime,
        "rcvtime" => $rcvtime
    );

    sleep($delay);

    echo "event: sendtime\n";
    echo "data:" . json_encode($data);
    echo "\n\n";

    while (ob_get_level() > 0) {
        ob_end_flush();
    }
    @flush;
}
?>