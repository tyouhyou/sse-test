<?php
if (empty($_GET)) {
    header("Location: /index.html" ) ;
    header("Cache-Control: no-cache");
} else {
    header("HTTP/1.1 200 OK");
    echo json_encode("response: sse-test");
    echo "\n\n";
}
?>